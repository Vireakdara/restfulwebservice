package com.example.demo.Service;

import com.example.demo.Model.Artical;
import com.example.demo.Respository.ArticalRespository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ArticalServiceImp implements ArticalService {

    ArticalRespository articalRespository;

@Autowired
    public void setArticalRespository(ArticalRespository articalRespository) {
        this.articalRespository = articalRespository;
    }

    @Override
    public List<Artical> findAll() {
        return articalRespository.findAll();
    }

    @Override
    public void delete(int id) {
        articalRespository.delete(id);
    }

    @Override
    public void update(int id, Artical artical) {
        articalRespository.update(id,artical);
    }

    @Override
    public void insert(Artical artical) {
        articalRespository.insert(artical);
    }
}
