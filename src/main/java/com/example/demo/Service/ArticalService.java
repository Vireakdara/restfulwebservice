package com.example.demo.Service;

import com.example.demo.Model.Artical;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@Repository
public interface ArticalService  {
    List<Artical> findAll();
    void delete(int id);
    void update(int id, Artical artical);
    void insert(Artical artical);
}
