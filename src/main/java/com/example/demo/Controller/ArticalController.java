package com.example.demo.Controller;

import com.example.demo.Model.Artical;
import com.example.demo.Service.ArticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article")

public class ArticalController {
    ArticalService articalService;

    @Autowired
    public ArticalController(ArticalService articalService) {
        this.articalService = articalService;
    }

    @GetMapping()
    public ResponseEntity<List<Artical>> findAll(){
        return new ResponseEntity<>(articalService.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<String> addNew(@RequestBody Artical artical){
        articalService.insert(artical);
        return new ResponseEntity<>("Added", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Artical artical){
        articalService.update(id, artical);
        return ResponseEntity.ok("Updated");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        articalService.delete(id);
        return ResponseEntity.ok("Deleted");
    }
}
