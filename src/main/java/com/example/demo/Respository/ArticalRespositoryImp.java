package com.example.demo.Respository;

import com.example.demo.Model.Artical;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticalRespositoryImp implements ArticalRespository {
    List<Artical> articales = new ArrayList<>();
    {
        articales.add(new Artical(1,"Harry Potter","It's a dog","Fiction"));
        articales.add(new Artical(2,"Fairy Tail","Fairy Law","Fiction"));
    }

    @Override
    public List<Artical> findAll() {
        return articales;
    }

    @Override
    public void delete(int id) {
        articales.remove(id);
    }

    @Override
    public void update(int id, Artical artical) {
        Artical artical1 = articales.get(id);
        artical1.setId(artical.getId());
        artical1.setName(artical.getName());
        artical1.setDescription(artical.getDescription());
        artical1.setCategory(artical.getCategory());
        articales.add(artical1);
    }

    @Override
    public void insert(Artical artical) {
        articales.add(artical);
    }
}
