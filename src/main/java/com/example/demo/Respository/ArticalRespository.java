package com.example.demo.Respository;

import com.example.demo.Model.Artical;

import java.util.List;

public interface ArticalRespository {

    List<Artical> findAll();
    void delete(int id);
    void update(int id, Artical artical);
    void insert(Artical artical);
}
